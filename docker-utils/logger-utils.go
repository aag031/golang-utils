package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/integrii/flaggy"
	"golang.org/x/net/context"
)

type sessionConfig struct {
	containerName string
}

// VERSION application version
const VERSION = "0.0.4"

func createLogFileName(containerName string) (string, string) {
	t := time.Now()
	timestamp := t.Format("2006-01-02-15-04-05")
	stdoutLogName := "container-log-stdout-" + containerName + "-" + timestamp + ".log"
	stderrLogName := "container-log-stderr-" + containerName + "-" + timestamp + ".log"
	fmt.Println("log : " + stdoutLogName)
	return stdoutLogName, stderrLogName
}

func getLogFile4Container(client *client.Client, containerName string) {
	logName, errLogName := createLogFileName(containerName)
	containerLogFile, errStdOutFileCreate := os.OpenFile(logName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	containerErrLogFile, errStdErrFileCreate := os.OpenFile(errLogName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if errStdOutFileCreate != nil {
		panic(errStdOutFileCreate)
	}

	if errStdErrFileCreate != nil {
		panic(errStdErrFileCreate)
	}

	ctx := context.Background()
	options := types.ContainerLogsOptions{ShowStdout: true}
	var logsContentReader io.ReadCloser
	var errGetLog error
	logsContentReader, errGetLog = client.ContainerLogs(ctx, containerName, options)
	if errGetLog != nil {
		panic(errGetLog)
	}

	_, errCopy := stdcopy.StdCopy(containerLogFile, containerErrLogFile, logsContentReader)
	if errCopy != nil && errCopy != io.EOF {
		panic(errCopy)
	}

	checkAndDeleteErrFile(containerErrLogFile)
}

func checkAndDeleteErrFile(errFile *os.File) {
	fmt.Println("checking stderr file size")
	fileInfo, errStat := (*errFile).Stat()
	if errStat != nil {
		panic(errStat)
	}
	size := fileInfo.Size()
	fmt.Println("Err size : ", size)
	if size == 0 {
		errFile.Close()
		fmt.Println("Remove " + errFile.Name())
		errRemove := os.Remove(errFile.Name())
		if errRemove != nil {
			fmt.Println(errRemove)
		}
	}
}

func parseCommandLine() (sessionConfig, error) {
	flaggy.SetName("Docker Utility Helper")
	flaggy.SetDescription("A little programm that help getting data from container")
	flaggy.DefaultParser.ShowHelpOnUnexpected = true
	flaggy.SetVersion(VERSION)
	var containerName string
	getLogSubCommand := flaggy.NewSubcommand("get")
	getLogSubCommand.Description = "getting log from container"
	getLogSubCommand.String(&containerName, "c", "container", "provide name of container for command logs")
	flaggy.AttachSubcommand(getLogSubCommand, 1)

	dataConfig := sessionConfig{}
	flaggy.Parse()
	if getLogSubCommand.Used {
		if len(containerName) == 0 {
			flaggy.ShowHelpAndExit("No container name in command line.Option 'c' or 'container 'is missed")
		}

		dataConfig.containerName = containerName
	} else {
		flaggy.ShowHelpAndExit("No command in comand line. I don't know what shall I do")
	}

	return dataConfig, nil
}

func getContainerList(client *client.Client) []string {
	ctx := context.Background()
	optionsList := types.ContainerListOptions{}
	containerList, errList := client.ContainerList(ctx, optionsList)
	if errList != nil {
		panic(errList)
	}

	var containerNameList []string
	for _, container := range containerList {
		containerNameList = append(containerNameList, container.Names[0][1:len(container.Names[0])])
	}

	return containerNameList
}

func main() {
	dataConfig, error := parseCommandLine()
	if error != nil {
		fmt.Println(error)
		os.Exit(1)
	}

	fmt.Println("Version : " + VERSION)
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	if dataConfig.containerName == "all" {
		containerNameList := getContainerList(cli)
		for _, containerName := range containerNameList {
			fmt.Println("Getting log for container " + containerName)
			getLogFile4Container(cli, containerName)
		}
	} else {
		fmt.Println("Getting log for container " + dataConfig.containerName)
		getLogFile4Container(cli, dataConfig.containerName)
	}
}
