package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/integrii/flaggy"
)

const VERSION = "0.0.4"

type sessionConfig struct {
	filterContent string
	fileName      string
	printMode     bool
	saveMode      bool
}

var configData sessionConfig

func parseCommandLine() (sessionConfig, error) {
	flaggy.SetName("Copy a lot files with error handling")
	flaggy.SetDescription("A little programm that read source folder and set to destination using a few streams")
	flaggy.DefaultParser.ShowHelpOnUnexpected = true
	flaggy.SetVersion(VERSION)
	var filterContent string
	var fileName string
	printSubCommand := flaggy.NewSubcommand("print")
	saveSubCommand := flaggy.NewSubcommand("save")
	printSubCommand.Description = "print all variable line by line"
	saveSubCommand.Description = "save environment context in file"
	printSubCommand.String(&filterContent, "f", "filter", "provide the filter ov variable name")
	saveSubCommand.String(&filterContent, "f", "filter", "provide the filter ov variable name")
	saveSubCommand.String(&fileName, "d", "dataset", "provide the filter ov variable name")
	flaggy.AttachSubcommand(printSubCommand, 1)
	flaggy.AttachSubcommand(saveSubCommand, 1)

	flaggy.Parse()
	if printSubCommand.Used {
		if len(filterContent) == 0 {
			fmt.Println("Prints all variable")
		}

		configData.filterContent = filterContent
		configData.fileName = ""
		configData.printMode = true
		configData.saveMode = false
	} else if saveSubCommand.Used {
		if len(filterContent) == 0 {
			fmt.Println("Prints all variable")
		}

		if len(fileName) == 0 {
			hostName, err := os.Hostname()
			if err != nil {
				flaggy.ShowHelpAndExit("Hmm ... could not get host name")
			}
			fileName = hostName + ".txt"
		}

		configData.filterContent = filterContent
		configData.fileName = fileName
		configData.printMode = false
		configData.saveMode = true
	} else {
		flaggy.ShowHelpAndExit("No command in command line. I don't know what shall I do")
	}
	return configData, nil
}

func removeLastSemicolon(line string) string {
	var result string
	if strings.HasSuffix(line, string(os.PathListSeparator)) {
		result = strings.TrimSuffix(line, string(os.PathListSeparator))
		return result
	}

	return line
}

func printEnvContext(list []string) {
	for idx, line := range list {
		fmt.Printf("%d : %s\n", idx, line)
	}
}

func saveEnvContext(list []string, fileName string) {
	fileObject, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	defer fileObject.Close()
	if err != nil {
		fmt.Errorf("Hmm ... Could not create file for saving enviroment variables")
		fmt.Println(err)
		return
	}
	for idx, line := range list {
		fmt.Fprintf(fileObject, "%d : %s\n", idx, line)
	}
}

func createEnvContext(list []string) []string {
	var resultList = make([]string, 0)
	for idx, line := range list {
		idx = idx + 1
		line = removeLastSemicolon(line)
		if strings.HasPrefix(line, "=") {
			continue
		}
		if strings.Contains(line, string(os.PathListSeparator)) {
			splittedValue := strings.Split(line, "=")
			splittedLongLine := splitLongValue(splittedValue[1], string(os.PathListSeparator), idx)
			fmt.Printf("%d : %s\n", idx, splittedValue[0])
			for _, lineIn := range splittedLongLine {
				resultList = append(resultList, lineIn)
			}
		} else {
			resultList = append(resultList, line)
		}
	}

	return resultList
}

func applyFilter(list []string, filter string) []string {
	var filteredList = make([]string, 0)
	var filterEnv = regexp.MustCompile(filter)
	for _, line := range list {
		tmp := strings.Split(line, "=")
		if filterEnv.MatchString(tmp[0]) {
			filteredList = append(filteredList, line)
		}
	}

	return filteredList
}

func splitLongValue(line string, sep string, idx int) []string {
	result := make([]string, 0)
	splittedString := strings.Split(line, sep)
	for idx2, value := range splittedString {
		result = append(result, fmt.Sprintf("%d.%d %s", idx, idx2, value))
	}

	return result
}

func main() {
	configData, _ := parseCommandLine()
	envList := os.Environ()
	if configData.filterContent != "" {
		envList = applyFilter(envList, configData.filterContent)
	}

	context := createEnvContext(envList)
	if configData.printMode {
		printEnvContext(context)
	}

	if configData.saveMode {
		saveEnvContext(context, configData.fileName)
	}
}
