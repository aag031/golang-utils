package main

import (
	"container/list"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

const VERSION = "0.0.1"

var cioEventQueue list.List
var folEventQueue list.List

// CioEventObject describes Customer Installation Event
type CioEventObject struct {
	EventID       string
	EventSource   string
	EventStream   string
	EventDateTime string
	EventType     string
	EventMetaData []string
	EventVersion  string
	EventPayload  interface{}
}

// FolEventObject describes Customer Installation Event
type FolEventObject struct {
	EventID       string
	EventSource   string
	EventStream   string
	EventDateTime string
	EventType     string
	EventMetaData []string
	EventVersion  string
	EventPayload  interface{}
}

type versionQueueResponse struct {
	Version string
}

type infoQueueResponse struct {
	Code int
}

type queueSizeResponse struct {
	Size int
}

func getCioEvent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if cioEventQueue.Len() == 0 {
		empty := infoQueueResponse{http.StatusNoContent}
		json.NewEncoder(w).Encode(empty)
	} else {
		event := cioEventQueue.Front()
		json.NewEncoder(w).Encode(event.Value)
		cioEventQueue.Remove(event)
	}
}

func getFolEvent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if folEventQueue.Len() == 0 {
		empty := infoQueueResponse{http.StatusNoContent}
		json.NewEncoder(w).Encode(empty)
	} else {
		event := folEventQueue.Front()
		json.NewEncoder(w).Encode(event.Value)
		folEventQueue.Remove(event)
	}
}

func postCioEvent(w http.ResponseWriter, r *http.Request) {
	var event CioEventObject
	decodeErr := json.NewDecoder(r.Body).Decode(&event)
	var response infoQueueResponse
	var flag bool
	response, flag = handleJsonParserError(decodeErr)
	if flag {
		response = updateQueue(cioEventQueue, &event, nil)
	}
	json.NewEncoder(w).Encode(response)
}

func postFolEvent(w http.ResponseWriter, r *http.Request) {
	var event FolEventObject
	decodeErr := json.NewDecoder(r.Body).Decode(&event)
	var response infoQueueResponse
	var flag bool
	response, flag = handleJsonParserError(decodeErr)
	if flag {
		response = updateQueue(folEventQueue, nil, &event)
	}
	json.NewEncoder(w).Encode(response)
}

func updateQueue(queue list.List, cio *CioEventObject, fol *FolEventObject) (response infoQueueResponse) {
	if cio != nil {
		queue.PushFront(cio)
	}
	if fol != nil {
		queue.PushFront(fol)
	}
	info := infoQueueResponse{http.StatusOK}
	return info
}

func handleJsonParserError(err error) (infoQueueResponse, bool) {
	if err != nil {
		fmt.Println(err)
		fmt.Println("Hmm ... Could not decode input request")
		return infoQueueResponse{http.StatusBadRequest}, false
	} else {
		return infoQueueResponse{http.StatusOK}, true
	}
}

func getCioEventQueueSize(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	sizeResponse := queueSizeResponse{cioEventQueue.Len()}
	fmt.Println(sizeResponse)
	json.NewEncoder(w).Encode(sizeResponse)
}

func getFolEventQueueSize(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	sizeResponse := queueSizeResponse{cioEventQueue.Len()}
	fmt.Println(sizeResponse)
	json.NewEncoder(w).Encode(sizeResponse)
}

func getEventQueueVersion(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	version := versionQueueResponse{"0.0.1"}
	fmt.Println(version)
	json.NewEncoder(w).Encode(version)
}

func main() {
	cioEventQueue.Init()
	folEventQueue.Init()
	r := mux.NewRouter()
	r.HandleFunc("/cioeventqueue", getCioEvent).Methods("GET")
	r.HandleFunc("/cioeventqueue/size", getCioEventQueueSize).Methods("GET")
	r.HandleFunc("/foleventqueue", getFolEvent).Methods("GET")
	r.HandleFunc("/foleventqueue/size", getFolEventQueueSize).Methods("GET")
	r.HandleFunc("/version", getEventQueueVersion).Methods("GET")
	r.HandleFunc("/cioeventqueue", postCioEvent).Methods("POST")
	log.Fatal(http.ListenAndServe(":8000", r))
}
