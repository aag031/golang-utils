package main 

import (
	"fmt"
)

type neuron struct {
	DendriteList [] float32
	Axon float32 
}

//
//Create new instance of neuron objects
// dendriteListSize - size of input vector
// return pointer to new instance
func NewNeuron(dendriteListSize int) (*neuron) {
	var tmpDendriteList = make([] float32, dendriteListSize)
	ptr := neuron {tmpDendriteList, 0}
	return &ptr
}

func main() {
	var neuronInstancePtr = NewNeuron(32)
	var neuronInstance = *neuronInstancePtr

	fmt.Println("Dendrite list size : ", len(neuronInstance.DendriteList))
	fmt.Println("Axon value : ", neuronInstance.Axon) 
}