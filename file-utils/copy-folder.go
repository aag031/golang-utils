package main

import (
	"bufio"
	"fmt"
	"github.com/integrii/flaggy"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

const VERSION = "0.0.1"

type sessionConfig struct {
	sourceFolder string
	destFolder   string
	excludeList []string
	excludeListSize int
}

var configData sessionConfig
var pathSeparator = string(os.PathSeparator)
var errorListFile *os.File
var logListFile *os.File
var errLogCreated error

func parseCommandLine() error {
	flaggy.SetName("Copy a lot files with error handling")
	flaggy.SetDescription("A little programm that read source folder and set to destination using a few streams")
	flaggy.DefaultParser.ShowHelpOnUnexpected = true
	flaggy.SetVersion(VERSION)
	var sourceFolder string
	var destFolder string
	var excludeFile string
	copySubCommand := flaggy.NewSubcommand("copy")
	copySubCommand.Description = "copy set of files"
	copySubCommand.String(&sourceFolder, "s", "source", "provide name of source folder")
	copySubCommand.String(&destFolder, "d", "dest", "provide name of destination folder")
	copySubCommand.String(&excludeFile, "x", "exclude", "provide name of file with exclude patterns")
	flaggy.AttachSubcommand(copySubCommand, 1)

	flaggy.Parse()
	if copySubCommand.Used {
		if len(sourceFolder) == 0 {
			flaggy.ShowHelpAndExit("No source folder name in command line.Option 's' or 'source 'is missed")
		}

		if len(destFolder) == 0 {
			flaggy.ShowHelpAndExit("No destination folder name in command line.Option 'd' or 'dest 'is missed")
		}

		if !strings.HasSuffix(destFolder, pathSeparator) {
			destFolder = destFolder + pathSeparator
		}

		if !strings.HasSuffix(sourceFolder, pathSeparator) {
			sourceFolder = sourceFolder + pathSeparator
		}

		configData.sourceFolder = sourceFolder
		configData.destFolder = destFolder
		configData.excludeList = readExcludeList(excludeFile)
		configData.excludeListSize = len(configData.excludeList)
	} else {
		flaggy.ShowHelpAndExit("No command in comand line. I don't know what shall I do")
	}

	return nil
}

func readExcludeList(fileName string) [] string {
	var list = make([]string,0)
	fmt.Println("Read : ", fileName)
	f, err := os.OpenFile(fileName, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("open file error: %v", err)
		return list
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	for {
		line, err := rd.ReadString('\n')
		line = strings.TrimSpace(line)
		fmt.Printf("Line : '%s'\n", line)
		if err != nil {
			if err == io.EOF {
				list = append(list, line)
				break
			}
			panic(err)
		}
		list = append(list, line)
	}

	sort.Strings(list)
	return list
}

func readSourceDir() {
	fmt.Println("Start reading source folder ... ", configData.sourceFolder)
	err := filepath.Walk(configData.sourceFolder, filePathCallBack)
	if err != nil {
		panic(err)
	}
}

func filePathCallBack(path string, fileInfo os.FileInfo, err error) error {
	if err != nil {
		fmt.Fprintln(errorListFile, err)
		return err
	}

	if fileInfo.Mode().IsRegular() {
		relativePath := strings.TrimPrefix(path, configData.sourceFolder)
		fmt.Println("Start processing of : ", path)
		fmt.Fprintln(logListFile, "Start processing of : ", path)
		go copyFile(path, relativePath, fileInfo.Name())
	} else {
		dirName := filepath.Base(path)
		fmt.Println("Check folder : ", dirName)
		idx := sort.SearchStrings(configData.excludeList, dirName)
		fmt.Println("idx : ", idx)
		fmt.Println("No open as file continue")
		if idx < configData.excludeListSize {
			fmt.Fprintln(errorListFile, "skipped : ", path)
			fmt.Fprintln(logListFile, "skipped : ", path)
			return filepath.SkipDir
		}
	}

	return nil
}

func copyFile(path string, relativePath string, fileName string) {
	fmt.Fprintln(logListFile, "Write file ... ", path)
	targetPath := configData.destFolder + relativePath
	targetBasePath := filepath.Dir(targetPath)
	if _, err := os.Stat(targetBasePath); os.IsNotExist(err) {
		if err := os.MkdirAll(targetBasePath, 0777); err != nil {
			fmt.Fprintln(errorListFile, err)
		}
	}
	output, errOutOpen := os.Create(targetPath)
		if errOutOpen != nil {
		fmt.Fprintln(errorListFile, errOutOpen)
		return
	}
	input, errOpenIn := os.Open(path)
	if errOpenIn != nil {
		fmt.Fprintln(errorListFile, errOpenIn)
	}
	written, errCopy := io.Copy(output, input)
	fmt.Fprintln(logListFile, "File : ", targetPath, " Written : ", written)
	if errCopy != nil {
		fmt.Fprintln(errorListFile, errCopy)
	}

	defer output.Close()
	defer input.Close()
}

func main() {
	errorListFile, errLogCreated = os.OpenFile("error-list.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
	if errLogCreated != nil {
		panic(errLogCreated)
	}
	logListFile, errLogCreated = os.OpenFile("log-list.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
	if errLogCreated != nil {
		panic(errLogCreated)
	}
	err := parseCommandLine()
	if err != nil {
		panic(err)
	}

	fmt.Print("Check destination folder ...")
	if _, err := os.Stat(configData.destFolder); err != nil {
		fmt.Println("  Not found. will be created")
		err := os.Mkdir(configData.destFolder, 0777)
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println("  OK")
	}
	readSourceDir()
}
